﻿#include <iostream>
#include <conio.h>
#include "Human.h"
#include "Man.h"
#include "Baby.h"
#include "Woman.h"

using namespace std;

int main()

{
    setlocale(LC_ALL, "");
    cout << "Создание класса Human: " << endl;
    Human human;

    cout << "human.Print :" << endl;

    human.Print();
    cout << endl;

    // cout << "Вызов конструктора с параметром Human(16, 50)" << endl;

    Human* kek = new Human(16, 50, 186);
    kek->Print();

    cout << "Вызван метод kek->Run(3)" << endl;

    kek->Run(3);
    kek->Print();

    cout << "Вызван метод kek->GetAge()" << endl;
    cout << kek->GetAge() << endl;

    cout << "Вызван метод kek->GetWeight()" << endl;
    cout << kek->GetWeight() << endl;

    cout << "Удаление kek" << endl;
    delete(kek);


    cout << "Создание объекта класса Man:" << endl;
    Man huh;
    cout << "Вызов Man.Print" << endl;
    huh.Print();
    cout << "Вызов метода SetPower(7)" << endl;

    huh.SetPower(7);
    huh.Print();

    Man* lol = new Man(18, 60, 186, 5);
    lol->Print();

    Human* pew = new Man(); //
    pew->Run(4);
    pew->Print();
    cout << "______________________" << endl;

    cout << "Приведение типов" << endl;
    cout << "Вызов Print с помощью приведения типов:((Man*)pew)->Print()" << endl;
    ((Man*)pew)->Print();

    cout << "Удаляем объект по адресу pew : delete pew" << endl;
    delete pew;

    cout << "______________________" << endl;

    cout << "Динамическое создание класса Human с помощью конструктора с параметрами: Human* humanDynamic = new Human(36,65,170) " << endl;
    Human* humanDynamic = new Human(36, 65, 170);
    cout << "Динамическое создание класса Baby с помощью конструктора с параметрами: Baby* babyDynamic = new Baby(36,65,170,3,9) " << endl;
    Baby* babyDynamic = new Baby(36, 65, 170, 3, 9);

    cout << "Значение humanDynamic: " << endl;
    humanDynamic->Print();

    cout << "Значение babyDynamic: " << endl;
    babyDynamic->Print();

    cout << "______________________" << endl;
    cout << "Композиция классов: " << endl;
    cout << "Создание объекта класса Woman* wom= new Woman()" << endl;

    Woman* wom = new Woman();

    cout << "Вызовем wom -> GetParam() -> GetAge()" << endl;
    wom->GetParam()->GetAge();

    cout << "Вызовем wom -> GetParam() -> GetWeight()" << endl;
    wom->GetParam()->GetWeight();

    cout << "Вызовем wom -> GetParam() -> GetHeight()" << endl;
    wom->GetParam()->GetHeight();

    cout << "Значение human: " << endl;
    human.Print();

    cout << "Вызовем wom->SetParam()" << endl;
    wom->SetParam(human);

    cout << "Вызовем wom -> GetParam() -> GetAge()" << endl;
    cout << wom->GetParam()->GetAge() << endl;

    cout << "Вызовем wom -> GetParam() -> GetWeight()" << endl;
    cout << wom->GetParam()->GetWeight() << endl;

    cout << "Вызовем wom -> GetParam() -> GetHeight()" << endl;
    cout << wom->GetParam()->GetHeight() << endl;

    human.Run(2);
    wom->SetParam(human);

    cout << "Вызовем wom -> GetParam() -> GetAge()" << endl;
    cout << wom->GetParam()->GetAge() << endl;

    cout << "Вызовем wom -> GetParam() -> GetWeight()" << endl;
    cout << wom->GetParam()->GetWeight() << endl;

    cout << "Вызовем wom -> GetParam() -> GetHeight()" << endl;
    cout << wom->GetParam()->GetHeight() << endl;

    wom->GetVozr();
    delete wom;


}
