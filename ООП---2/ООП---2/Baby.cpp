#include "Baby.h"
#include <iostream>

using namespace std;

Baby::Baby() :Man() {

    dex = 0;

    cout << "����� ������������ �� ��������� Baby :: Baby()" << endl;
}

Baby::Baby(int a, int w, int h, int p, int d) : Man(a, w, h, p) {

    dex = d;
    cout << "����� ������������ � ����������� Baby :: Baby(" << age << "," << weight << "," << height << "," << power << "," << dex << ")" << endl;
}

void Baby::Print() {
    Man::Print();
    cout << "�������� ��������" << " " << dex << endl;
}

Baby::Baby(Baby& baby) :Man(baby) {

    dex = baby.dex;
}

void Baby::SetDex(int d) {

    dex = d;
}

Baby :: ~Baby() {

    cout << "����� ����������� ������ Baby" << endl;
}