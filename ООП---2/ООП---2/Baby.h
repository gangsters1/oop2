#pragma once
#include "Man.h"

class Baby : public Man {
protected:
    int dex;
public:
    Baby();
    Baby(int a, int w, int h, int p, int d);
    Baby(Baby& Black);

    void Print();
    void SetDex(int d);

    ~Baby() override;

}; 