#pragma once
#include "Human.h"

class Man : public Human {
protected:
	int power;
public:
	Man();
	Man(int a, int w, int h, int p);
	Man(Man& Black);

	void Print();
	void SetPower(int p);

	~Man() override;
	
};