#include "human.h"
#include <iostream>


using namespace std;

Human::Human() {
	age = 0;
	weight = 0;
	height = 0;

	cout << "����� ������������ �� ��������� ������ Human : Human()" << endl;
}

Human::Human(int a, int w, int h) {
	age = a;
	weight = w;
	height = h;

	cout << "����� ������������ � ���������� ������ Human : Human(" << age << "," << weight << ","<<height<<")" << endl;
}

Human::Human(Human& black) {
	age = black.age;
	weight = black.weight;
	height = black.height;

	cout << "����� ������������ ����������� ������ Human : Human(Human& gay)" << endl;
}

void Human::Run(int w) {
	weight -= w;

	cout << "����� ������ Run (" << weight << ")" << endl;
}

void Human::Print() {

	cout << "�������� age: " << age << " // �������� weight: " << weight << " // �������� height: " << height << endl;

}

int Human::GetAge() {

	return age;

}

int Human::GetWeight() {

	return weight;

}

int Human::GetHeight() {

	return height;
	
}

Human :: ~Human() {

	cout << "����� ����������� ~Human()" << endl;
}