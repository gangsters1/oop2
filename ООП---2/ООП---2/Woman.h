#pragma once
#include <iostream>
#include "Human.h"

class Woman {

private:
	Human* param;	

public:
	Woman();
	Woman(Human& age1);
	Woman(Woman& woman);

	void GetVozr();
	Human* GetParam();
	void SetParam(Human& _param);

	~Woman();
};