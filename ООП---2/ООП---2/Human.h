#pragma once

class Human {
protected:
	int age, weight, height;

public:
	Human();
	Human(int a, int w, int h);
	Human(Human& Black);

	void Run(int w);
	void Print();
	int GetAge();
	int GetWeight();
	int GetHeight();

	virtual ~Human();

};
